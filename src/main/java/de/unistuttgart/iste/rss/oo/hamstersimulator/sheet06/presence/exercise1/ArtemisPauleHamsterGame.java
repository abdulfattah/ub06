package de.unistuttgart.iste.rss.oo.hamstersimulator.sheet06.presence.exercise1;

import de.unistuttgart.iste.rss.oo.hamstersimulator.external.model.SimpleHamsterGame;

/**
 * This class is used to solve some easy tasks and to show how Artemis works.
 */
public class ArtemisPauleHamsterGame extends SimpleHamsterGame {

	/**
	 * Creats a new ArtemisPauleHamsterGame do not modify
	 */
	public ArtemisPauleHamsterGame() {
		this.loadTerritoryFromResourceFile("/territories/ArtemisPaule.ter");
		this.displayInNewGameWindow();
		game.startGame();

	}

	/**
	 * Put the hamster code into this method.
	 */
	@Override
	protected void run() {
		pickOddGrains();
		turnRight();
		paule.move();
		turnRight();
		pickEvenGrains();
		paule.turnLeft();
		paule.move();
		paule.turnLeft();
		pickAllGrains();
	}

	private void pickAllGrains() {
		for (int i = 0; i < 6; i++) {

			paule.pickGrain();

			paule.move();
		}

	}

	private void pickEvenGrains() {
		for (int i = 0; i < 6; i++) {
			if (paule.frontIsClear()) {
				paule.move();

			}
			if (i % 2 == 0) {
				paule.pickGrain();
			}
			
		}

	}

	private void turnRight() {
		paule.turnLeft();
		paule.turnLeft();
		paule.turnLeft();
	}

	private void pickOddGrains() {
		for (int i = 0; i < 6; i++) {

			if (i % 2 == 1) {
				paule.pickGrain();
			}
			if (paule.frontIsClear()) {
				paule.move();

			}
		}

	}
}
