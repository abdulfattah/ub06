package de.unistuttgart.iste.rss.oo.hamstersimulator.sheet06.homework.exercise1;

import de.unistuttgart.iste.rss.oo.hamstersimulator.external.model.SimpleHamsterGame;

/**
 * This Class paints spirals no matters in which Square or Rectangle
 * 
 * @author Moneeb Adel, st168568@stud.uni-stuttgart.de, 3479230
 * @author Abdulsamad Abdulfattah, st159779@stud.uni-stuttgart.de, 3332720
 * @version (02)
 */
public class PainterPauleHamsterGameB extends SimpleHamsterGame {

	/**
	 * Creates a new PainterPauleHamsterGameB do not modify
	 */
	public PainterPauleHamsterGameB() {
		this.loadTerritoryFromResourceFile("/territories/PainterPauleBTerritory.ter");
		this.displayInNewGameWindow();
		game.startGame();
	}

	/**
	 * Run Method which calls some another Methods and lets them interacting with an
	 * Algorithm to paint a Spiral
	 * 
	 * 
	 */
	@Override
	protected void run() {
		measureLength();
		measureWidth();
		while (!paule.mouthEmpty()) {
			if (turnCounter < 2) {
				smartTurnLeft();
				moveNTimesPaint(stepsLengthCounter);

			} else {
				stepsLengthCounter--;
				stepsLengthCounter--;
				stepsWidthCounter--;
				stepsWidthCounter--;
				paule.turnLeft();
				moveNTimesPaint(stepsWidthCounter);
				paule.turnLeft();
				moveNTimesPaint(stepsLengthCounter);
			}
		}
	}

	// an Integer to recognize how long is the Length
	int stepsLengthCounter = 0;
	// an Integer to recognize how long is the Width
	int stepsWidthCounter = 0;
	// an Integer to recognize how many times did Paule turn Left
	int turnCounter = 0;

	/*
	 * Paule turns Left and saves how many times he turned to the Left
	 * 
	 * @requires paule.isInitialized;
	 * 
	 * @requires is.Initialized;
	 * 
	 * @ensures Paule turned to the Left
	 * 
	 * @ensures old(turnCounter) < turnCounter
	 */
	void smartTurnLeft() {
		paule.turnLeft();
		turnCounter++;
	}

	/*
	 * The Method lets Paule move steps Times
	 * 
	 * @requires !paule.mouthEmpty;
	 * 
	 * @requires paule.frontIsClear;
	 * 
	 * @ensures Paule moved steps Times
	 * 
	 * @ensures there less Corns in Paules Mouth
	 */
	void moveNTimesPaint(int steps) {
		/*
		 * @loop_inviant Paule visited steps Tiles
		 * 
		 * @decreasing steps-i
		 */
		for (int i = 0; i < steps; i++) {
			if (!paule.mouthEmpty()) {
				paule.putGrain();
			}
			if (!paule.mouthEmpty()) {
				paule.move();
			}
		}
	}

	/*
	 * This Method is has the same Code in the run Method It lets Paule to paint a
	 * Spiral in any Square and Rectangle
	 * 
	 * @requires Paule starts at the Begin of the Length Side, not in the middle or
	 * anything like that
	 * 
	 * @requires Paule looks in a direction that lets him makes circles in
	 * anticlockwise rotation
	 * 
	 * @requires paule.isInitialized;
	 * 
	 * @requires isInitialized;
	 * 
	 * @ensures Paule painted a Spiral
	 * 
	 * @ensures Paule has no Corns in his Mouth
	 */
	public void paintSpiral() {
		measureLength();
		measureWidth();
		while (!paule.mouthEmpty()) {
			if (turnCounter < 2) {
				// get ready to minimize the go in the Spiral
				smartTurnLeft();
				moveNTimesPaint(stepsLengthCounter);

			} else {
				// minimize the Sizes of the Length and Width to rich the Spirals Form
				stepsLengthCounter--;
				stepsLengthCounter--;
				stepsWidthCounter--;
				stepsWidthCounter--;
				paule.turnLeft();
				moveNTimesPaint(stepsWidthCounter);
				paule.turnLeft();
				moveNTimesPaint(stepsLengthCounter);

			}
		}
	}

	/*
	 * get the Size of the Length
	 * 
	 * @requires Paule starts at the Begin of the Length Side, not in the middle or
	 * anything like that
	 * 
	 * @requires paule.isInitialzed;
	 * 
	 * @requires stepsLengthCounter=0;
	 * 
	 * @ensures stepsLengthCounter > 0
	 * 
	 * @ensures Paule moved (stepsLengthCounter) times
	 */
	private void measureLength() {
		while (paule.frontIsClear()) {
			paule.putGrain();
			paule.move();
			stepsLengthCounter++;
		}
	}

	/*
	 * get the Size of the Width
	 * 
	 * @requires the left Side is opened
	 * 
	 * @requires paule.isInitialzed;
	 * 
	 * @requires stepsWidthCounter=0;
	 * 
	 * @ensures stepsWidthCounter > 0
	 * 
	 * @ensures Paule moved (stepsLengthCounter) times
	 */
	void measureWidth() {
		smartTurnLeft();
		while (paule.frontIsClear()) {
			paule.putGrain();
			paule.move();
			stepsWidthCounter++;
		}

	}
}
